class Character:
    def __init__(self, name, beschreibung, aktueller_raum):
        self.name = name
        self.beschreibung = beschreibung
        self.gegenstaende = []
        self.aktueller_raum = aktueller_raum

    # def setName(self, name):
    #   self.__name = name

    # def getName(self):
    #   return self.__name

    def gegenstandNehmen(self, neuer_gegenstand):
        neuer_gegenstand = self.aktueller_raum.gibGegenstand(neuer_gegenstand)  
        self.gegenstaende = self.gegenstaende + [neuer_gegenstand]   
          
    
    def gegenstandZeigen(self):
        print("Dein Rucksack beinhaltet folgende Gegenstände")
        for item in self.gegenstaende:
            print(item.name, end = " | ") 

class Spieler(Character):
    
    def __init__(self, name, beschreibung, aktueller_raum):
        super().__init__(name, beschreibung, aktueller_raum)
        #self.aktueller_raum = aktueller_raum
        self.teilzielGegenstand = False
        self.teilzielRaum = False   
    
    def raumWechseln(self, richtung, Orte):
        richtung = " ".join(richtung)
        if "nord" in richtung and self.aktueller_raum.ausgaenge[0] >= 0:
            neuer_raum = Orte[self.aktueller_raum.ausgaenge[0]]
            # print(neuer_raum)
            if neuer_raum.name == "Küche" and self.teilzielGegenstand == False:
                print("Du musst noch die Gegenstände finden.")
                return
            else:
                self.teilzielRaum = True
        elif "ost" in richtung and self.aktueller_raum.ausgaenge[1] >= 0:
            neuer_raum = Orte[self.aktueller_raum.ausgaenge[1]]
        elif "süd" in richtung and self.aktueller_raum.ausgaenge[2] >= 0:
            neuer_raum = Orte[self.aktueller_raum.ausgaenge[2]]
        elif "west" in richtung and self.aktueller_raum.ausgaenge[3] >= 0:
            neuer_raum = Orte[self.aktueller_raum.ausgaenge[3]]
        else:
            print("In diese Richtung gibt es keinen Ausgang")
            neuer_raum = self.aktueller_raum
                      
        self.aktueller_raum = neuer_raum
      
        print("Du befindest dich im Raum",self.aktueller_raum.name)
    
    def gegenstandVerwenden(self):
        pass
    
    def gegenstandNehmen(self, neuer_gegenstand):
        super().gegenstandNehmen(neuer_gegenstand)
        #Hier wird das Teilziel Gegenstand geprüft
        itemnamen = []
        for item in self.gegenstaende:
            itemnamen = itemnamen + [item.name]
        if "Kleiner Schlüssel" in itemnamen and "Großer Schlüssel" in itemnamen:
            self.teilzielGegenstand = True
                
    def raumUntersuchen(self):
        self.aktueller_raum.beschreibeRaum()