class Room:
    def __init__(self, name, beschreibung, gegenstaende, ausgaenge):
        self.name = name
        self.__beschreibung = beschreibung
        self.gegenstaende = gegenstaende
        self.ausgaenge = ausgaenge
    
    def beschreibeRaum(self):
        print("Du befindest dich in", self.name)
        print(self.__beschreibung)
        print("Im Raum befinden sich folgende Gegenstände:")
        for item in self.gegenstaende:
            print("*"*10, item.name, sep="\n")
    
    def gibGegenstand(self,gegenstand):
      index = self.gegenstaende.index(gegenstand)
      return self.gegenstaende.pop(index)
