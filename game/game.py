from rooms import Room as Raum
from items import Item as Gegenstand
from characters import Spieler

class Game:

  def __init__(self, inventory, locations, player):
    self.codeWordDic = {
                    "richtungen": ["nord", "norden", "ost", "osten", "süd", "süden", "west", "westen"], 
                    "bewegung": ["gehe","geh", "bewege"],
                    "nutzen": ["nutze", "nutzen"],
                    "untersuchen": ["zeige", "umschauen", "untersuchen"],
                    "nehmen":["nimm","sammel", "nehmen"]
                    }
    # self.richtungen = ["nord", "norden", "ost", "osten", "süd", "süden", "west", "westen"]
    # self.bewegungRaum = ["gehe","geh"]
    # self.interaktionInventar = ["nutze", "nutzen"]
    # self.zeige = ["zeige", "umschauen", "untersuchen"]
    self.locations = locations
    self.inventory = inventory
    self.player = player
    
  def addInventoryCodeWordDic(self):
    key = "items"
    values = []
    for item in self.inventory:
      values = values + [item.name.lower()]
      # print(values)
    self.codeWordDic.update({key: values})


    #self.codeWordDic.update({key,values})
    
  def checkVocabs(self, eingabe):
    # eingabe = self.strInList(eingabe)
    ergebnis =  []
    for key, value in self.codeWordDic.items():
      ergebnis = ergebnis + self.intersectionList(eingabe,value)
      if self.intersectionList(eingabe,value):
        ergebnis = ergebnis + [key]
    return ergebnis

  # Diese Funktion erzeugt eine Liste, dabei ist jedes Wort ein Element der Liste.
  def strInList(self, eingabe):
      eingabe = eingabe.lower()
      eingabe = eingabe.split()
      return eingabe

  # Diese Funktion vergleicht zwei Listen auf gemeinsame Einträge. Diese Einträge werden als Liste ausgegeben.
  def intersectionList(self, string, liste):
      a = set(self.strInList(string))
      b = set(liste)
      return list(a.intersection(b))

  def startGame(self):
      self.addInventoryCodeWordDic()
      # print(self.codeWordDic)

      while True:
        eingabe = input("Was möchtest du tun? \n").lower()
        pEingabe = self.checkVocabs(eingabe)
        # pEingabe = eingabe 

        # geheInRichtung = self.intersectionList(eingabe, self.vocabs["richtungen"])
        # geheInRichtung = "".join(geheInRichtung)

        # print(geheInRichtung)    
        print(pEingabe)
        if eingabe == "ende":
            break
        elif "richtungen" in pEingabe and "bewegung" in pEingabe:
            self.player.raumWechseln(pEingabe, self.locations)
        elif "items" in pEingabe and "nehmen" in pEingabe:
          for item in self.player.aktueller_raum.gegenstaende:
            if item.name.lower() in pEingabe:
              self.player.gegenstandNehmen(item)
              self.player.gegenstandZeigen()
        elif "untersuchen" in pEingabe and "items" in pEingabe:
          for item in (self.player.aktueller_raum.gegenstaende + self.player.gegenstaende):
            if item.name.lower() in pEingabe:
              item.beschreibeGegenstand()
        elif "untersuchen" in pEingabe:
            self.player.raumUntersuchen()
        

        elif eingabe == "hilfe":
            print("Hier kommen einige Tipps")
            #self.player.raumUntersuchen()
        else:
            print("Leider verstehe ich deine Eingabe nicht")

        # Ziel des Spiels    
        if self.player.teilzielGegenstand and self.player.teilzielRaum:
            print("Du hast das Spiel gewonnen")
            break

  def saveGame(self):
    pass

  def winGame(self):
    pass

  def __del__(self):
    print("Das Spiel wurde beendet")



goldSchluessel = Gegenstand("Goldschlüssel", "Mit dem Goldschlüssel lässt sich ein Schloss der Tür öffnen.")
silberSchluessel = Gegenstand("Silberschlüssel", "Mit dem Silberschlüssel kann man das große Schloss öffnen")

inventar = [goldSchluessel, silberSchluessel]

wohnzimmer = Raum("Wohnzimmer", "Ein gemühtliches Zimmer", [silberSchluessel, goldSchluessel], [1,-1,-1,-1])
kueche = Raum("Küche", "Ein Raum mit Essen", [],[-1,-1,0,-1])

# Die Räume werden in die Liste Orte geschrieben.
orte = [wohnzimmer, kueche]

print("Willkommen zum Textadventure-Beispiel \n")

eingabe = input("Wie möchtest du heißen? \n")

# Instanzierung des Spielers
spieler = Spieler(eingabe, "Beschreibung", wohnzimmer)

# Das Spiel wird gestartet
dasSpiel = Game(inventar, orte, spieler)

print("Hallo", spieler.name, "\n Du befindest dich gerade in dem Raum", spieler.aktueller_raum.name, "\n hier warten einige Aufgaben auf dich. Du musst zwei Gegenstände finden und in die Küche kommen, da du großen Hunger hast.")

dasSpiel.startGame()