# Schritt für Schritt: Textadventure (OOA und OOD)

Diese Anleitung gibt Ihnen die Möglichkeit, Schritt für Schritt anhand eines kleinen Beispiels die objektorientierte Analyse und das objektorientierte Design kennen zu lernen. Nutzen Sie für eine Vertiefung die zusammengestellten [Informationsquellen](https://moodle.itech-bs14.de/mod/page/view.php?id=71394).

## Das Szenario
In dem Szenario sollten Sie den Rahmen festlegen. Grundsätzlich gibt es Räume (gerne auch Schauplätze genannt), Gegenstände und Charaktere (oder Kreaturen). Darüber hinaus sollte klar werden, wann das Spiel beendet wird und was man als Spieler hierfür tun muss.

Folgendes Beispiel liegt der Einführung zugrunde:
__Sie sind Tom und befinden sich im Wohnzimmer. Im Norden befindet sich ein weiteres Zimmer (Küche). Das Ziel des Spiels ist es, in die Küche zu kommen. Allerdings ist die Tür verschlossen. Um die Tür zu öffnen muss man zwei Schlüssel im Wohnzimmer finden. Hat man die Küche betreten, ist das Spiel beendet.__


## Die OOA und Use-Case-Diagramme

Bevor Sie an die Implementierung denken, steht zunächst die objektorientierte Analyse (OOA) an.

### Ziele: 

- Anforderungen an das zu entwickelnde Softwaresystem erfassen und beschreiben
- ein gemeinsames Verständnis der Produktidee bei Auftraggeber, Auftragnehmer, Benutzer und Entwicklern schaffen

### Tätigkeiten:
- Herausarbeiten der wesentlichen Sachverhalte, Vernachlässigen von Details
- Dokumentation von Anwendungsfällen (Szenarien)
- Dokumentation der Systemidee, der Leistungsmerkmale, der Rahmenbedingungen und Voraussetzungen 
- Entwickeln eines objektorientierten Analysemodells (z.B. USE-CASE-Diagramm)

### Umsetzung:
- als Text und/oder als Modell 
- weitgehend unabhängig von der späteren konkreten Umsetzung
- Modellierung oft mit Elementen der Unified Modeling Language (UML)

Das folgende Use-Case-Diagramm zeigt Ihnen die wesentlichen Anwendungsfälle, die sich aus dem Text ableiten lassen. 
Der Use-Case "Charakter treffen" wird in diesem Beispiel nicht weiter behandelt. Dies können Sie in Ihren eigenen Szenarien berücksichtigen.

**Tipp:** Wie Sie ein Use-Case-Diagramm lesen und erstellen, finden Sie [hier](https://moodle.itech-bs14.de/course/view.php?id=1020).

![Use-Case](./img/usecase.png)

## OOD

Nachdem Sie die wesentlichen Aspekte Ihres Textadventures festgelegt haben. Können Sie an das s.g. objektorientierte Design gehen (OOD).

### Ziele:
- Erarbeitung eines Lösungskonzeptes zu einem gegebenen Problem oder gegebenen Anforderungen
- Dokumentation des Konzeptes als Vorarbeit für die Implementierung

### Tätigkeiten:
- das in der OOA erstellte Analysemodell wird kontinuierlich weiterentwickelt und, darauf aufbauend, ein objektorientiertes Designmodell erstellt (z.B. Klassendiagramm)
- Festlegung von Verantwortlichkeiten, Schnittstelen und Implementierungsvorgaben
- Dokumentation von Klassen, Objekten, Komponenten (u.v.m.) und deren Interaktion

### Umsetzung: 
- Modellierung in der Regel mit Elementen der Unified Modeling Language (UML)
- Das Designmodell berücksichtigt neben den fachlichen Aspekten des Auftraggebers auch technische Gegebenheiten (Betriebssystem, Zielsprache etc.)

Wir konzentrieren uns beim OOD auf Klassen- und Objektdiagramme. Wahlweise lassen sich auch Sequenzdiagramme gut umsetzen, um eine zeitliche Komponente zu planen.

### Klassendiagramme

Da Klassen einen Bauplan darstellen, der allgemein gehalten werden sollte, können wir nicht einfach die einzelnen "Objekte" aus dem Szenario als Klassen übernehmen. Wir müssen "Gruppen" bilden oder eine Verallgemeinerung finden.

[Hier](https://moodle.itech-bs14.de/course/view.php?id=580) finden Sie wichtige Hinweise zur Erstellung von Klassen.

Aus dem Szenario lassen sich nun folgende Klassen ableiten. 

- Tom ist eine Person. Sollen weitere Figuren implementiert werden, kann eine allgemeinere Kategorie gefunden werden: Dies könnte z.B. Charakter oder Person sein.
- Wohnzimmer und Küche sind Räume. Dies ist im Grunde schon die Klasse: Raum.
- Die beiden Schlüssel können als Gegenstände aufgefasst werden und bilden damit die Klasse: Gegenstand.

Person, Raum und Gegenstand sind unsere Klassen, die wir in einem Klassendiagramm darstellen. In der unteren Abbildung sehen wir allerdings noch zwei weitere Klassen Spieler und Charakter. Der Pfeil deutet darauf hin, dass die Klassen Spieler und Charakter alle Attribute und Methoden von der Klasse Person **erben**. Person ist somit die übergeordnete Klasse (Super-Klasse), Spieler und Charakter sind s.g. Unterklassen (Supklassen).

![klassen.PNG](./img/klasse_1.png)

### Assoziationen (Beziehungen)

Klassen "kennen" häufig andere Klassen, dies ist jedoch nicht erforderlich. Beispielsweise weiß eine Person, in welchem Raum sie sich gerade befindet. Dies ist eine einfache Beziehung (**Assoziation**), die wir mit einer einfachen Verbindungsline symbolisieren.

Interessant wird es bei den Gegenständen, da sich diese immer in einem Raum oder bei einer Person befinden. Ein Gegenstand brauch somit einen "Träger". Dies ist eine s.g. **Aggregation**. Dies nennt man auch Ganzes-Teil-Beziehung. Das Symbol ist die Raute. Diese zeigt immer zum "Ganzen".

Eine weitere Beziehungsform ist die **Komposition**. Dazu können sie [hier](https://moodle.itech-bs14.de/course/view.php?id=580) mehr erfahren, diese ist in diesem Beispiel nicht relevant.

**Aus dem Bild wird somit deutlich, dass die Klasse Person, die Klasse Raum kennt. Ein Gegenstand ist immer Teil eines Raums oder des Spielers. Spieler und Charakter erben von der Klasse Person und kennen somit auch die Klasse Raum.**

### Attribute und Methoden
Nun müssen Sie sich über die Eigenschaften (Attribute) und die Methoden (Aktivitäten) einer Klasse Gedanken machen. Diese lassen sich gut aus dem Use-Case-Diagramm ableiten.

#### Person
Was charakterisiert eine Person?
- Sie besitzt einen Namen,
- sie hat eine Beschreibung,
- sie befindet sich immer in einem Raum und
- sie besitzt einen oder mehrere Gegenstände.

Was kann eine Person machen?
- Sie kann einen Gegenstand nehmen.
- Sie kann einen Gegenstand verwenden.
- Sie kann einen Raum wechseln.
- Sie kann einen Raum untersuchen.

#### Raum

Was charakterisiert einen Raum?
- Er hat einen Namen.
- Er hat eine Beschreibung.
- Er hat Ausgänge, die zu anderen Räumen führen.
- In dem Raum befinden sich Gegenstände.

#### Gegenstände
Was charakterisiert einen Gegensand?
- Er hat einen Namen.
- Er hat eine Beschreibung.

Aus dieser Bestandsaufnahme lassen sich nun die Attribute und Methoden der einzelnen Klassen herausarbeiten.

![klasse2.PNG](./img/klasse_2.png)

## Zur Implementierung

schauen wir uns an, wie die drei Klassen in Python aussehen.

### Hilfsfunktionen
Hier fangen wir mit einigen Hilfsfunktionen zur Textverarbeitung an. Die Aufgabe lässt sich auch ohne diese bearbeiten. Außerdem gibt es viel ausgefeiltere Lösungen.


```python
# Diese Funktion erzeugt eine Liste, dabei ist jedes Wort ein Element der Liste.
def strInList(eingabe):
    eingabe = eingabe.lower()
    eingabe = eingabe.split()
    return eingabe

# Diese Funktion vergleicht zwei Listen auf gemeinsame Einträge. Diese Einträge werden als Liste ausgegeben.
def intersectionList(string, liste):
    a = set(strInList(string))
    b = set(liste)
    return list(a.intersection(b))
```

### Definition der Klassen und Instanzierung der Objekte

Die Klasse Gegenstand wird mit den Eigenschaften Name und Beschreibung definiert. Es ist denkbar der Klasse auch Methoden zu geben. Beispielsweise kann der Spieler die Methode beschreibe Gegenstand nutzen.


```python
# Klasse Gegenstand
class Gegenstand:
    def __init__(self, name, beschreibung):
        self.name = name
        self.beschreibung = beschreibung
        
    def beschreibeGegenstand(self):
        pass
```

Der Bauplan (Klasse) Gegenstand wird dafür genutzt, zwei Objekte zu initialisieren. Es sind ein kleiner und ein großer Schlüssel.


```python
# Instanzierung der Gegenstände
kleinerSchluessel = Gegenstand("Kleiner Schlüssel", "Mit dem kleinen Schlüssel lässt sich ein Schloss der Tür öffnen.")
grosserSchluessel = Gegenstand("Großer Schlüssel", "Mit dem großen Schlüssel kann man das große Schloss öffnen")

# Testen
kleinerSchluessel.name
```




    'Kleiner Schlüssel'



Die Klasse Raum wird definiert:

Ein Raum hat einen Namen, eine Beschreibung, eventuell Gegenstände und Ausgänge.
Name und Beschreibung sind als String definiert Gegenstände werden bei der Instanzierung als Liste übergeben Auch die Ausgänge werden als Liste übergeben. Gibt es einen Übergang? Dann gilt, dass es max. 4 Übergänge gibt: Nord, Ost, Süd und West. In der Reihenfolge wird folgende Liste übergeben: [-1,1,-1,-1]. Diese Darstellng bedeutet, dass der Ostausgang in den Raum mit der Nummer 1 führt. Bei Nord, Süd und West gibt es keine Übergänge. Dies wird mit der -1 bezeichnet.


```python
class Raum:
    def __init__(self, name, beschreibung, gegenstaende, ausgaenge):
        self.name = name
        self.__beschreibung = beschreibung
        self.gegenstaende = gegenstaende
        self.ausgaenge = ausgaenge
    
    def beschreibeRaum(self):
        print("Du befindest dich in", self.name)
        print(self.__beschreibung)
        print("Im Raum befinden sich folgende Gegenstände:")
        for item in self.gegenstaende:
            print("* ", item.name, sep="\n")
```

Instanzierung der Räume + Übergabe der Gegenstände, die vorher definiert wurden und der Ausgänge.


```python
wohnzimmer = Raum("Wohnzimmer", "Ein gemühtliches Zimmer", [grosserSchluessel, kleinerSchluessel], [1,-1,-1,-1])
kueche = Raum("Küche", "Ein Raum mit Essen", [],[-1,-1,0,-1])

# Die Räume werden in die Liste Orte geschrieben.
Orte = [wohnzimmer, kueche]
# Testen

wohnzimmer.gegenstaende[0].name
```




    'Großer Schlüssel'



Definition der Klasse Person


```python
class Person:
    def __init__(self, name, beschreibung, aktueller_raum):
        self.name = name
        self.beschreibung = beschreibung
        self.gegenstaende = []
        self.aktueller_raum = aktueller_raum
    
    def gegenstandNehmen(self, neuer_gegenstand):
        self.gegenstaende = self.gegenstaende + [neuer_gegenstand]        
    
    def gegenstandZeigen(self):
        print("Dein Rucksack beinhaltet folgende Gegenstände")
        for item in self.gegenstaende:
            print(item.name, end = " | ") 
```

Definition der Klasse Spieler. **Diese Klasse erbt alles von der der Klasse Person**.
Die methode **raumWechseln()** ist wie folgt zu verstehen.
Es wurd eine Richtung als Wert übergeben. Stimmt der Wert mit einer der Himmelsrichtungen überein und ist in der Liste der Ausgänge keine -1 eingetragen, wird der aktuelle_raum auf den neuen Raum aus der Liste "Orte" gesetzt.


```python
# Die Klasse Spieler erbt alle Attribute und Methoden der Klasse Person.
class Spieler(Person):
    
    def __init__(self, name, beschreibung, aktueller_raum):
        super().__init__(name, beschreibung, aktueller_raum)
        #self.aktueller_raum = aktueller_raum
        self.teilzielGegenstand = False
        self.teilzielRaum = False   
    
    def raumWechseln(self,richtung):
        if "nord" in richtung and self.aktueller_raum.ausgaenge[0] >= 0:
            neuer_raum = Orte[self.aktueller_raum.ausgaenge[0]]
            # print(neuer_raum)
            if neuer_raum.name == "Küche" and self.teilzielGegenstand == False:
                print("Du musst noch die Gegenstände finden.")
                return
            else:
                self.teilzielRaum = True
        elif "ost" in richtung and self.aktueller_raum.ausgaenge[1] >= 0:
            neuer_raum = Orte[self.aktueller_raum.ausgaenge[1]]
        elif "süd" in richtung and self.aktueller_raum.ausgaenge[2] >= 0:
            neuer_raum = Orte[self.aktueller_raum.ausgaenge[2]]
        elif "west" in richtung and self.aktueller_raum.ausgaenge[3] >= 0:
            neuer_raum = Orte[self.aktueller_raum.ausgaenge[3]]
        else:
            print("In diese Richtung gibt es keinen Ausgang")
            neuer_raum = self.aktueller_raum
                      
        self.aktueller_raum = neuer_raum
        
        
            
        print("Du befindest dich im Raum",self.aktueller_raum.name)
    
    def gegenstandVerwenden(self):
        pass
    
    def gegenstandNehmen(self, neuer_gegenstand):
        super().gegenstandNehmen(neuer_gegenstand)
        #Hier wird das Teilziel Gegenstand geprüft
        itemnamen = []
        for item in self.gegenstaende:
            itemnamen = itemnamen + [item.name]
        if "Kleiner Schlüssel" in itemnamen and "Großer Schlüssel" in itemnamen:
            self.teilzielGegenstand = True
                
    def raumUntersuchen(self):
        self.aktueller_raum.beschreibeRaum()
```

### Das Spiel beginnt hier
Es ist vorstellbar eine eigene Klasse Spiel/Game etc. zu erzeugen. Dies ist jedoch für diesen Zweck nicht erforderlich. 


```python
print("Willkommen zum Textadventure-Beispiel \n")

eingabe = input("Wie möchtest du heißen? \n")

# Instanzierung des Spielers
spieler = Spieler(eingabe, "Beschreibung", wohnzimmer)

print("Hallo", spieler.name, "\n Du befindest dich gerade in dem Raum", spieler.aktueller_raum.name, "\n hier warten einige Aufgaben auf dich. Du musst zwei Gegenstände finden und in die Küche kommen, da du großen Hunger hast.")

while True:
    eingabe = input("Was möchtest du tun? \n").lower()
    
    richtungen = ["nord", "norden", "ost", "osten", "süd", "süden", "west", "westen"]
    
    geheInRichtung = intersectionList(eingabe, richtungen)
    geheInRichtung = "".join(geheInRichtung)
        
    if eingabe == "ende":
        print("Das Spiel wird beendet")
        break
    elif geheInRichtung in richtungen:
        spieler.raumWechseln(geheInRichtung)
    elif eingabe == "hilfe":
        print("Hier kommen einige Tipps")
        spieler.raumUntersuchen()
    elif eingabe == "nimm":
        spieler.gegenstandNehmen(kleinerSchluessel)
        spieler.gegenstandNehmen(grosserSchluessel)
        print(spieler.gegenstandZeigen())
    else:
        print("Leider verstehe ich deine Eingabe nicht")
    
    # Ziel des Spiels    
    if spieler.teilzielGegenstand and spieler.teilzielRaum:
        print("Du hast das Spiel gewonnen")
        break
```

    Willkommen zum Textadventure-Beispiel 
    
    


```python
spieler.aktueller_raum.name
```

## Links
Hier finden Sie weitere hilfreiche Links:

- Tutorial Teil 1: https://bmu-verlag.de/text-adventure-teil-1-ein-eigenes-abenteuerspiel-schreiben/
- Tutorial Teil 2: https://bmu-verlag.de/text-adventure-teil-2-der-code/
